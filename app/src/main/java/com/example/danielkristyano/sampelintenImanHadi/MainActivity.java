package com.example.danielkristyano.sampelintenImanHadi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.danielkristyano.sampelintendanielkristyanto.R;

public class MainActivity extends AppCompatActivity
{
    private Button btnSub1, btnSub2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSub1 = (Button)findViewById(R.id.button1);
        btnSub2 = (Button)findViewById(R.id.button2);


        btnSub1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SubMainKalkulator.class);
                startActivity(intent);
            }
        });

        btnSub2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SubMainIntent.class);
                startActivity(intent);
            }
        });

    }
}

